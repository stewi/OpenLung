# CONCEPT 11: Clamshell
---
## Scope: Air-piston driven BVM Compressor
 - Retain all advantages of OxVent (direct pressure manipulation, simplicity)
 - Add ability to hot-swap and/or manually vent with BVM (pop open clamshell or Y-valve a manual bag)
 - Remove need for Compressed Air/O2 Supply with motor-driven piston
 - Decrease footprint with custom clamshell that contains UI and can be hung on IV stand. 
 - Piston/motor/HW could be collocated or separated by air hose.
 
## Team Lead(s):

| Team     | Gitlab   | Slack            |
| ---      |  ------  |-----------------:|
|  Acme    |    @jd18 | @ Jonathan Kemp   |
|         |           |               |


## Slack Channels: 
- #compairconceptventilator
- #design-hardware

## Issue Labels:
-

## Current Collab Docs:

## Overview:
* A simple form-fitting two-piece "Clamshell" printed or fabricated close to geometry of BVM in place of the box in Concept 10 (reduces latency, airflow needs, quickly producible/buildable in mass Quantities)
* Motor-driven air piston used to pressurize the vessel, eliminating need for compressed air (or compressed air input as an option)
* Compressed Air "spring" is largely conserved and balanced with a mechanical spring, minimizing strain on the motor used to move the Piston.
* Packaging allows entire assembly to have one cleanable shell (only I/O are control interface, vent tubes, power cable) and could hang on IV stand or any other cart. 
* Direct, reliable control of BVM Pressure/Volume
* no bearings or gears. Only replacement/maintenance is the piston seals, probably beyond timeframe of current crisis

### Considerations:
* Injection mold clamshell for mass production
* Air piston needs better packaging
